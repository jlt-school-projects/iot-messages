'use strict';

var clientFromConnectionString = require('azure-iot-device-mqtt').clientFromConnectionString;
var Message = require('azure-iot-device').Message;

var connectionString = 'HostName=saintpri-pri2.azure-devices.net;DeviceId=JulesLaurent2Device;SharedAccessKey=KOW0JIrG0s+siSHkdlrY/EICQ8Tzh1gyGvzbPGRHdSU=';
var client = clientFromConnectionString(connectionString);

function printResultFor(op) {
    return function printResult(err, res) {
        if (err) console.log(op + ' error: ' + err.toString());
        if (res) console.log(op + ' status: ' + res.constructor.name);
    };
}

var connectCallback = function (err) {
    if (err) {
        console.log('Could not connect: ' + err);
    } else {
        console.log('Client connected');

        // Create a message and send it to the IoT Hub every second
        setInterval(function(){
            var nb_male = 20 + (Math.random() * 15);
            var nb_female = 60 + (Math.random() * 20);
            var data_json = {
                    type: 'in',
                    nb_male: nb_male,
                    nb_female:nb_female
                };
            var message = new Message(JSON.stringify(data_json));
            console.log("Sending message: " + message.getData());
            client.sendEvent(message, printResultFor('send'));
        }, 1000);
    }
};

client.open(connectCallback);
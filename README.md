# iot-messages
Small node js project to send messages to iot hub.

## Installation 
```
npm install 
```
In each subdir 


## Initialisation
1 - In createdeviceidentity/CreateDeviceIdentity.js and receivemessages/ReadDeviceToCloudMessages.js replace the following line :
```
var connectionString = '{yourconnectionstring}';
```

2-
```
node createdeviceidentity/CreateDeviceIdentity.js
```
keep the printed informations i.e
```
Device ID: MyDeviceID
Device key: KOW0JIrG0s+eiSHkdlrY/EICQ8Tzh1gyGvzbPGDHdSU=
```

3-
In simulateddevice/SimulatedDevice.js edit the following line as follow:
```
var connectionString = 'HostName={youriothostname};DeviceId={yourdeviceid};SharedAccessKey={yourdevicekey}';
```

